console.log('a =', a)
var a = 2
console.log('a =', a)

// igual
var b
console.log('b =', b)
b = 2
console.log('b =', b)

function teste() {
    console.log('a =', a)
    var a = 2
    console.log('a =', a)
}

teste()

// O efeito de issamento com o let não ocorre

