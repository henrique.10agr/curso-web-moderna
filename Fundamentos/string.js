const escola = "Cod3r"

console.log(escola.charAt(4))//cahrAt serve para pegar uma letra da string no caso aquela que está no indice 4
console.log(escola.charAt(5))
console.log(escola.charCodeAt(3))//Valor na tabela ASCI
console.log(escola.indexOf('3'))//Retor o indece em que o caracter 3 se encontra

console.log(escola.substring(1))//Imprime do indice um pra frente
console.log(escola.substring(0, 3))//Imprimw do indece 0 ao 2

console.log('Escola '.concat(escola).concat('!'))
console.log('Escola '+escola+'!')
console.log(escola.replace(3, 'e'))//Substitui tudo que for 3 por e

console.log("Maria,João,Pedro".split(','))