// var é a declaração de uma váriavel global que 
// só não pode ser acessada quando declarada detro de uma função
{
    {
        {
            { 
                var sera = "Será???" 
            }
        }
    }
}
console.log(sera)

function teste() {
    var local = 123
}