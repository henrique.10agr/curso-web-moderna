// Função sem retorno

function imprimirSoma(a, b) {
    console.log(a + b);
}

imprimirSoma(2,3)
imprimirSoma(2)
imprimirSoma(2, 10, 11, 32)
imprimirSoma()

// Função com retorno

function soma(a, b = 1) {// caso o valor de b não seja informado ele sera 1
    return a + b
}

console.log(soma(1, 2))
console.log(soma(2))
console.log(soma())