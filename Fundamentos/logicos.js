function compra(trabalho1, trabalho2) {
    const comprarSorvete = trabalho1 || trabalho2
    const comprarTv50 = trabalho1 && trabalho2
    // const comprarTv32 = !!(trabalho1 ^ trabalho2) // bitwise xor
    const comprarTv32 = trabalho1 != trabalho2 // Xor que é o ou exclusivo
    const manterSaudavel = !comprarSorvete
    
    // return {
    //     comprarSorvete: comprarSorvete, 
    //     comprarTv50: comprarTv50, 
    //     comprarTv32: comprarTv32, 
    //     manterSaudavel: manterSaudavel
    // }

    return { comprarSorvete, comprarTv50, comprarTv32, manterSaudavel } //Forma reduzida para evitar redundancia
}

console.log(compra(true, true))
console.log(compra(false, true))
console.log(compra(false, false))