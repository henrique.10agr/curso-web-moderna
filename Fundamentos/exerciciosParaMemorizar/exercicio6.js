// 6 - Calcule a área de Cone;
const raio = 5
const geratriz = 8

area = Math.PI*raio*(geratriz+raio)

console.log(`Área do cone: ${area.toFixed(2)}`) 