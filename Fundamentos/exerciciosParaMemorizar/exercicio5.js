// 5 - Calcule a área de Cilindro;

const altura = 8
const raio = 3

const diametro = raio*2
const circunferencia = diametro * Math.PI 
const area = altura*circunferencia

console.log(`Area do Cilindro: ${area.toFixed(2)}`)