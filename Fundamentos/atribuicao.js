const a = 7
let b = 3

b += a // 3 + 7
console.log(b)

b -= a // 10 - 7
console.log(b)

b *= a // 3 * 7
console.log(b)

b /= a // 21 / 7
console.log(b)

b %= 2 // 3 % 2 : se par 0 se impar 1
console.log(b)