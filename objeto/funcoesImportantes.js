const pessoa = {
    nome: "Rebeca",
    idade: 2,
    peso: 13
}

console.log(Object.keys(pessoa)) // Pega as chaves
console.log(Object.values(pessoa)) // Pega os Valores
console.log(Object.entries(pessoa))// Pega os Pares Chave Valor

Object.entries(pessoa).forEach(([chave, valor]) => {
    console.log(`${chave}: ${valor}`)
})

Object.defineProperty(pessoa, 'dataNascimento', {
    enumerable: true, // Pode Ser listada
    writable: false, // Pode ser modificada
    value: '01/01/2019' // Valor
})

pessoa.dataNascimento = '01/01/2017'
console.log("================\n", pessoa.dataNascimento)
console.log(Object.keys(pessoa))

// Object.assign (ECMAScript 2015)
const dest = { a: 1 }
const o1 = { b: 2 }
const o2 = { c: 3, a: 4 }
const obj = Object.assign(dest, o1, o2)
console.log(obj)

Object.freeze(obj)
obj.c = 1234
console.log(obj)