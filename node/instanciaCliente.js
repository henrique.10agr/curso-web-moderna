const contadorA = require('./instanciaUnica') // Aponta pro mesmo local 
const contadorB = require('./instanciaUnica') // Aponta pro mesmo local

const contadorC = require('./instanciaNova')() // Ele cria o objeto aqui
const contadorD = require('./instanciaNova')() // Ele cria o objeto aqui

contadorA.inc()
contadorA.inc()
console.log(contadorA.valor, contadorB.valor)

contadorC.inc()
contadorC.inc()
console.log(contadorC.valor, contadorD.valor)