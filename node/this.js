console.log(this === global)
console.log(this === module)


console.log(this === module.exports)
console.log(this === exports)

function logThis() {
    console.log('Dentro de uma função...')
    console.log(this === exports)
    console.log(this === module.exports)
    console.log(this === global)
    
    this.perigo = '...' //salvando no global
}

const logThis2 = () => {
    console.log('Dentro de uma Arrow função...')
    console.log(this === exports)
    console.log(this === module.exports)
    console.log(this === global)
    
    this.perigo = '...' //salvando no global
}

logThis()
logThis2()
