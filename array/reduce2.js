const alunos = [
    { nome: 'João', nota: 7.3, bolsista: false},
    { nome: 'Maria', nota: 9.2, bolsista: false},
    { nome: 'Pedro', nota: 9.8, bolsista: false},
    { nome: 'Ana', nota: 8.7, bolsista: false},
]

function situacaoBolsa (aluno) {
    return aluno.bolsista
}

const situacaoAlunos = alunos.map(situacaoBolsa)

function contaBolsistas (acumulador, atual) {
    if (atual) {
        acumulador++
    }
    return acumulador
}

// Desafio 1: Todos os alunos são bolsistas ?

function todosBolsistas(situacaoAlunos) {
    const numeroAlunos = situacaoAlunos.length
    const numeroBolsistas = situacaoAlunos.reduce(contaBolsistas, 0)
    return numeroAlunos === numeroBolsistas ? true : false
}

console.log(todosBolsistas(situacaoAlunos))


// Desafio 2: Algum aluno bolsista ?
function algumBolsista(situacaoAlunos) {
    const numeroBolsistas = situacaoAlunos.reduce(contaBolsistas, 0)
    return numeroBolsistas > 0 ? true : false
}

console.log(algumBolsista(situacaoAlunos))
    

