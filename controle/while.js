const geraNumeroAleatorio = function (max, min) {
    const valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}

let valor = 0

while(valor != -1) {
    valor = geraNumeroAleatorio(10, -1)
    console.log("O valor é: " + valor)
}

console.log("Até a Próxima")