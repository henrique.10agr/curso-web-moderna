const notas = [1,2,3,4,5,6,7,8,9,10]

for (let i in notas){
    if (i == 5) {
        break //para o laço mais próximo
    }
    console.log(i + ", " + notas[i])
}

for (let i in notas){
    if (i == 5) {
        continue //pula essa repetição do laço mais próximo
    }
    console.log(i + ", " + notas[i])
}

console.log("====================")

externo: for (let x in notas){
    for (let i in notas){
        if (x == 5 && i == 3) {
            break externo //para o laço com rótulo externo, NUNCA USAR :(
        }
        console.log(x, i)
    }    
}
