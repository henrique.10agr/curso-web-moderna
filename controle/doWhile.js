const getInteiroAleatorioEntre = function (min, max) {
    let valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}

let valor = -1

do {
    valor = getInteiroAleatorioEntre(-1, 10)
    console.log("O valor é: " + valor)
} while (valor != -1)

console.log("Até a próxima")
