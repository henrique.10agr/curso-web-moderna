let contador = 1
while (contador <= 10) {
    console.log(`Contador: ${contador}`)
    contador++
}

for(let i = 1; i <= 10; i++){
    console.log(`I: ${i}`)
}

const notas = [2.2, 4.3, 5.3, 6.7, 8.8, 9.9]
for (let i = 0; i < notas.length; i++) {
    console.log(notas[i])
}