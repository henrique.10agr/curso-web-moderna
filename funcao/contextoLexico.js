const valor = 'Global'

function minhaFuncao() { /* A função leva em consideração o local onde foi definida,*/ 
    console.log(valor)   /* quando definida 0 valor era igual a 'Global'*/
}

function exec() {
    const valor = 'Local'
    minhaFuncao()
}

exec()
