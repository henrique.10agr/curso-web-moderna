// Closure ele é o escopo cria quando uma função é declarada
// Esse escopo permite a função acessar e manipula variaveis esternas à função

// Cotexto léxico em ação!

const x = 'Global'

function fora() {
    const x = 'Local'
    function dentro() {
        return x
    }

    return dentro
}

const minhaFuncao = fora()
console.log(minhaFuncao())
