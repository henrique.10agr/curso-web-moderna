function area(altura, largura) {
    const area = altura * largura
    if (area > 20) {
        console.log(`Valor acima do permitido: ${area}m².`)
    } else{
        return area
    }
}

console.log(area(2))
console.log(area(2, 10))
console.log(area(2, 3, 23, 42, 24, 2))
console.log(area(5,5))