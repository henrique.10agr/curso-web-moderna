// Função em JS é First-Class Object (Citizens)
// Higher-order function

// Criar de forma Literal
function func1(/* Paramentros */) { /* Código */ }

// Armazenar em uma variável
const func2 = function (/* Paramentros */) { /* Código */ }

// Armazenar em um array
const array = [function func3(a,b) { return a + b }, func1, func2]
//            |              Não é comum          |
console.log(array[0](2, 3))

// Armazenar em um atributo de objeto
const obj = {}
obj.falar = function () { return "Opa" }
console.log(obj.falar())

// Passar função como param
function run(fun){
    fun()
}

run(function () {console.log('Executando...')})

// Uma função pode retornar/conter uma função
function soma(a, b){
    return function (c) {
        console.log(a+b+c)
    }
}

soma(2,3)(4)
const cincoMais = soma(2,3)
cincoMais(4)