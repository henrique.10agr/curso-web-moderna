const pessoa = {
    saudacao: 'Bom dia!!!',
    falar() {
        console.log(this.saudacao)// O this referencia a o objeto pessoa
    }
}

pessoa.falar()

const falar = pessoa.falar
falar() /* Nessa ocasiaõ o this váriou pois não está atrelado ao objeto
         * pessoa agora ele está referenciando outro objeto */

    
const falarDePessoa = pessoa.falar.bind(pessoa)

falarDePessoa() 