function criarProduto(nome, preco) {
    // this.nome = nome
    // this.preco = preco
    // this.desconto = 5
    // return {
    //     nome: nome,
    //     preco: preco,
    //     desconto: 5
    // }
    return {
        nome,
        preco,
        desconto: 0.1
    }
}

// const produto1 = new criarProduto("sabonete", 3.50)
const produto1 = criarProduto("sabonete", 3.50)
console.log(produto1.desconto)
